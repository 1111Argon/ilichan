class WelcomeController < ActionController::Base
  def index
    @name = 'Gabriel'
    @fee = "1,000"
    @logo = File.read(Rails.root.join("public", "LOGO.png"))
    @gogox_logo_url = "https://google.pt"
    @item = Item.new("Some Title", [ItemLine.new("Some Label", "TEEEEXT")])
    @items = [@item]
  end

  def index2
    @name = 'Gabriel'
  end
end
