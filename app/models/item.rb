class Item
    def initialize(title, items)
        @title = title
        @items = items
    end
    def title
        @title
    end
    def items
        @items = []
    end
end